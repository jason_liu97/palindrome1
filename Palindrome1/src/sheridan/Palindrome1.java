package sheridan;

public class Palindrome1 {
	public static boolean isPalindrome(String input) {
		
		input = input.toLowerCase().replaceAll(" ", "");
		
		for (int i = 0, j = input.length() - 1 ; i < j ; i++, j--) {
			if (input.charAt(i) != input.charAt(j)) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(" is anna palindrome? " + isPalindrome("anna"));
		System.out.println(" is race car palindrome? " + isPalindrome("race car"));
		System.out.println(" is taco cat palindrome? " + isPalindrome("taco cat"));
		System.out.println(" is this palindrome? " + isPalindrome("this is not palidrome"));
	}
}
